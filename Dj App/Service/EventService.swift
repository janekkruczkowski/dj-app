//
//  Service.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 07/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import Foundation

class EventService {

    static let shared = EventService()
    private (set) var events = [Event]()

    func fetch(completion: @escaping (_ completed: Error?) -> Void) {

        URLSession.shared.dataTask(with: URL(string: "https://djapp-test.herokuapp.com/")!) { (data, resp, err) in

            if let err = err {
                completion(err)
                return
            }
            guard let data = data else {
                completion(ApiError.noData)
                return
            }
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                decoder.dateDecodingStrategy = .iso8601
                let jsonData = try decoder.decode([Event].self, from: data)
                self.events = jsonData
                completion(nil)

            } catch {
                completion(error)
            }


        }.resume()


    }

    func requestInvitation(invitation: Invitation, completion: @escaping (_ data: InvitationResponse?, _ error: Error?) -> Void) {
        guard let url = URL(string: "https://djapp-test.herokuapp.com/invitation/get") else {
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        request.allHTTPHeaderFields = headers

        do {
            let data = try JSONEncoder().encode(invitation)
            request.httpBody = data
        } catch {
            completion(nil, error)
            print(error)

        }
        URLSession.shared.dataTask(with: request) { (data, resp, err) in
            if let err = err {
                print(err)
                completion(nil, err)
                return
            }
            guard let data = data else {
                return
            }
            do {
                let response = try JSONDecoder().decode(InvitationResponse.self, from: data)
                print(response)
                completion(response, nil)
            } catch {
                completion(nil, error)
                print(error)
            }


        }.resume()
    }
}
