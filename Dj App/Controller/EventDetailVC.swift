//
//  EventVC.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 05/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit

class EventDetailVC: UIViewController {

    let event: Event

    let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.isUserInteractionEnabled = true
        view.isScrollEnabled = true
        view.showsVerticalScrollIndicator = true
        view.showsHorizontalScrollIndicator = true
        return view
    }()
    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        return lbl
    }()
    let subTitleLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.7096532583, green: 0.7096532583, blue: 0.7096532583, alpha: 1)
        return lbl
    }()
    let startDateLbl: UILabel = {
        let lbl = UILabel()
        return lbl
    }()
    let descriptionLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        return lbl
    }()
    let image: UIImageView = {
        let img = UIImageView()
        img.contentMode = UIView.ContentMode.scaleAspectFill
        img.clipsToBounds = true
        return img
    }()


    init(event: Event) {
        self.event = event
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Szczegóły"
        setupParty()
        setupView()

    }

    func setupView() {
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalTo(view.safeAreaLayoutGuide)
        }
        scrollView.addSubview(image)
        image.snp.makeConstraints { (make) in
            make.top.equalTo(scrollView.snp.top).offset(16)
            make.leading.equalTo(view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(view.safeAreaLayoutGuide).inset(16)
            make.height.equalTo(180)
        }
        scrollView.addSubview(titleLbl)
        titleLbl.snp.makeConstraints { (make) in
            make.top.equalTo(image.snp.bottom).offset(16)
            make.leading.equalTo(view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(view.safeAreaLayoutGuide).inset(16)
        }
        scrollView.addSubview(subTitleLbl)
        subTitleLbl.snp.makeConstraints { (make) in
            make.top.equalTo(titleLbl.snp.bottom).offset(6)
            make.leading.equalTo(view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(view.safeAreaLayoutGuide).inset(16)
        }
        scrollView.addSubview(startDateLbl)
        startDateLbl.snp.makeConstraints { (make) in
            make.top.equalTo(subTitleLbl.snp.bottom).offset(6)
            make.leading.equalTo(view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(view.safeAreaLayoutGuide).inset(16)
        }
        scrollView.addSubview(descriptionLbl)
        descriptionLbl.snp.makeConstraints { (make) in
            make.top.equalTo(startDateLbl.snp.bottom).offset(16)
            make.leading.equalTo(view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(view.safeAreaLayoutGuide).inset(16)
            make.bottom.lessThanOrEqualTo(scrollView).inset(16)
        }


    }

    func setupParty() {
        titleLbl.text = event.name
        subTitleLbl.text = event.place.name
        descriptionLbl.text = event.description
        startDateLbl.text = "Start: \(event.getStartTimeString())"
        image.sd_setImage(with: URL(string: event.cover.source))

    }


}
