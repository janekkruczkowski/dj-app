//
//  EventsVC.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 05/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"
private let service: EventService = EventService.shared

class EventsVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    fileprivate let loadingIndicator = LoadingSpinner(frame: .zero)

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = #colorLiteral(red: 0.9198423028, green: 0.9198423028, blue: 0.9198423028, alpha: 1)
        title = "Wydarzenia"
        navigationController?.navigationBar.prefersLargeTitles = true

        // Register cell classes
        self.collectionView!.register(EventCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        setupSpinner(spinner: loadingIndicator)

        fetch()

    }

    fileprivate func fetch() {
        service.fetch() { [weak self] (err) in
            if err == nil {
                DispatchQueue.main.async {
                    self?.hideLoadingIndicator(spinner: self?.loadingIndicator)
                    self?.collectionView.reloadData()
                }

            } else {
                DispatchQueue.main.async {
                    self?.hideLoadingIndicator(spinner: self?.loadingIndicator)
                    self?.showError(error: err!)
                }

            }
        }
    }

    // MARK: UICollectionViewDataSource


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return service.events.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! EventCell
        cell.parentViewController = self
        cell.setupView(event: service.events[indexPath.item])
        // Configure the cell

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width - 32, height: 345)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 32
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 32, left: 0, bottom: 32, right: 0)
    }


    // MARK: ErrorHandling
    func showError(error: Error) {
        let label = ErrorLabel(message: error.localizedDescription)
        view.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.centerY.equalTo(view.safeAreaLayoutGuide)
            make.leading.equalTo(view.safeAreaLayoutGuide)
            make.trailing.equalTo(view.safeAreaLayoutGuide)
        }


    }


}
