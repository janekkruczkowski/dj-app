//
//  InvitationVC.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 06/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit

class InvitationVC: UIViewController {

    fileprivate let eventId: String

    let stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = UIStackView.Alignment.fill
        stack.distribution = UIStackView.Distribution.equalSpacing
        stack.spacing = 10
        return stack
    }()
    let nameTxtField: TextField = {
        let txt = TextField()
        txt.placeholder = "Imie"
        return txt
    }()
    let surnameTxtField: TextField = {
        let txt = TextField()
        txt.placeholder = "Nazwisko"
        return txt
    }()
    let actionBtn: UIButton = {
        let btn = Button(type: .system)
        btn.setTitle("Zapisz", for: .normal)
        return btn
    }()


    init(eventId: String) {
        self.eventId = eventId
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.title = "Dopisz do listy"
        setupView()
    }


    func setupView() {

        stackView.addArrangedSubview(nameTxtField)
        stackView.addArrangedSubview(surnameTxtField)
        stackView.addArrangedSubview(actionBtn)
        view.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(16)
            make.leading.equalTo(view.safeAreaLayoutGuide).offset(16)
            make.trailing.equalTo(view.safeAreaLayoutGuide).inset(16)
        }
        actionBtn.addTarget(self, action: #selector(actionBtnTapped), for: .touchUpInside)
        
        addGestureRecognizer()
    }


    @objc func actionBtnTapped() {
        if isFormValid() {
            requestInvitation(name: nameTxtField.text!, surname: surnameTxtField.text!)
        }
        
    }
    
    @objc func viewTapped(_ sender: UITapGestureRecognizer) {
     view.endEditing(true)
        
    }
    
    func isFormValid()-> Bool{
        guard let name = nameTxtField.text, name.count >= 3 else {
            showValidationError()
            return false
        }
        guard let surname = surnameTxtField.text, surname.count >= 3 else {
            showValidationError()
            return false
        }
        
        return true
        
    }
    
    func addGestureRecognizer(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func requestInvitation(name: String, surname: String){
        let invitation = Invitation.init(name: name, surname: surname, partyId: eventId)
        
        EventService.shared.requestInvitation(invitation: invitation) { [weak self] (res, err) in
            if let err = err {
                self?.showMessage(title: ERROR_TITLE, message: err.localizedDescription)
                return
            }
            if let res = res {
                if res.error == nil {
                    self?.showMessage(title: "Sukces", message: "Jesteś na liście")
                } else {
                    self?.showMessage(title: ERROR_TITLE, message: res.error!)
                }
                
            }
        }
        
    }
    

    func showValidationError() {
        showMessage(title: ERROR_TITLE, message: "Każde z pól musi zawierać minimum 3 znaki.")
    }

    func showMessage(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))

            self.present(alert, animated: true)
        }


    }


}
