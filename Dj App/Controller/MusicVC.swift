//
//  MusicVC.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 05/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit
import WebKit
import SnapKit

class MusicVC: UIViewController {

    fileprivate var webView: WKWebView!
    fileprivate let loadingIndicator = LoadingSpinner(frame: .zero)

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Muzyka"
        view.backgroundColor = .white
        setupWebView()
        setupSpinner(spinner: loadingIndicator)

    }


    fileprivate func setupWebView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self
        view.addSubview(webView)
        webView.snp.makeConstraints { (make) in
            make.edges.equalTo(view.safeAreaLayoutGuide)
        }
        let myURL = URL(string: "https://www.mixcloud.com/discover/david-guetta/")
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        webView.isHidden = true

    }

    fileprivate func showError(message: String) {
        let lbl = ErrorLabel(message: message)
        view.addSubview(lbl)
        lbl.snp.makeConstraints { (make) in
            make.centerY.equalTo(view.safeAreaLayoutGuide)
            make.left.equalTo(view.safeAreaLayoutGuide).offset(16)
            make.right.equalTo(view.safeAreaLayoutGuide).inset(16)
        }

    }


}

extension MusicVC: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideLoadingIndicator(spinner: loadingIndicator)
        webView.isHidden = false
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        hideLoadingIndicator(spinner: loadingIndicator)
        showError(message: "Error Occured. Check your internet connection and try again.")
    }

}
