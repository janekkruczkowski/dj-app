//
//  Invitation.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 07/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import Foundation


struct Invitation: Codable {
    let name: String
    let surname: String
    let partyId: String

}

struct InvitationResponse: Codable {
    let error: String?
}
