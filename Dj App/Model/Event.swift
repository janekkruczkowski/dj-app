//
//  Party.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 07/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import Foundation


struct Event: Decodable {
    let name: String
    let startTime: Date
    let endTime: Date
    let description: String
    let cover: Cover
    let id: String
    let place: Place

    func getStartTimeString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .medium
        dateFormatter.locale = Locale(identifier: "pl_PL")
        return dateFormatter.string(from: startTime)
    }

    func isiInvitationAvaliable() -> Bool {
        return startTime.addingTimeInterval(-3600) > Date()
    }
}

struct Cover: Decodable {
    let source: String

}

struct Place: Decodable {
    let name: String
    let location: Location
}

struct Location: Decodable {
    let city: String
    let country: String
    let latitude: Double
    let longitude: Double
    let street: String
    let zip: String

}
