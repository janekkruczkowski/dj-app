//
//  Button.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 06/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit

class Button: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        tintColor = .white
        layer.cornerRadius = 8
        clipsToBounds = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var intrinsicContentSize: CGSize {
        return .init(width: 100, height: 45)
    }

}
