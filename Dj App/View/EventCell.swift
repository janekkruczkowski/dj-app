//
//  EventCell.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 05/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit
import SDWebImage

class EventCell: UICollectionViewCell {

    var parentViewController: UIViewController? = nil
    var event: Event!

    let image: UIImageView = {
        let img = UIImageView()
        img.contentMode = UIView.ContentMode.scaleAspectFill
        img.clipsToBounds = true
        return img
    }()

    let titleLbl: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 2
        lbl.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        return lbl
    }()
    let subTitleLbl: UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.7096532583, green: 0.7096532583, blue: 0.7096532583, alpha: 1)
        return lbl
    }()

    let detailsBtn: Button = {
        let btn = Button(type: .system)
        btn.setTitle("Szczegóły", for: .normal)
        return btn
    }()

    let actionBtn: Button = {
        let btn = Button(type: .system)
        btn.setTitle("Dopisz się na listę", for: .normal)
        return btn
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white


    }

    func setupView(event: Event) {
        self.event = event
        layer.cornerRadius = 16
        clipsToBounds = true
        titleLbl.text = event.name
        subTitleLbl.text = event.place.name
        image.sd_setImage(with: URL(string: event.cover.source))
        addSubview(image)
        addSubview(titleLbl)
        addSubview(subTitleLbl)
        let btnStack = UIStackView()
        btnStack.spacing = 8
        btnStack.distribution = UIStackView.Distribution.fillEqually
        if event.isiInvitationAvaliable() {
            btnStack.addArrangedSubview(actionBtn)
            actionBtn.addTarget(self, action: #selector(actionBtnTapped), for: .touchUpInside)
        }
        btnStack.addArrangedSubview(detailsBtn)
        addSubview(btnStack)

        image.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(16)
            make.leading.equalTo(self).offset(16)
            make.trailing.equalTo(self).inset(16)
            make.height.equalTo(180)
        }

        titleLbl.snp.makeConstraints { (make) in
            make.top.equalTo(image.snp.bottom).offset(8)
            make.leading.equalTo(snp.leading).offset(16)
            make.trailing.equalTo(snp.trailing).inset(16)
        }

        subTitleLbl.snp.makeConstraints { (make) in
            make.top.equalTo(titleLbl.snp.bottom).offset(2)
            make.leading.equalTo(snp.leading).offset(16)
            make.trailing.equalTo(snp.trailing).inset(16)
        }
        btnStack.snp.makeConstraints { (make) in
            make.top.equalTo(subTitleLbl.snp.bottom).offset(8)
            make.leading.equalTo(snp.leading).offset(16)
            make.trailing.equalTo(snp.trailing).inset(16)
            make.bottom.equalTo(snp.bottom).inset(16)
        }


        detailsBtn.addTarget(self, action: #selector(detailsBtnTapped), for: .touchUpInside)
    }

    @objc func actionBtnTapped() {
        self.parentViewController?.navigationController?.pushViewController(InvitationVC(eventId: event.id), animated: true)


    }

    @objc func detailsBtnTapped() {

        self.parentViewController?.navigationController?.pushViewController(EventDetailVC(event: event), animated: true)


    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
