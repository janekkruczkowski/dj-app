//
//  ErrorLabel.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 05/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit

class ErrorLabel: UILabel {

    init(message: String) {
        super.init(frame: .zero)
        self.text = message
        self.numberOfLines = 0
        self.textAlignment = .center
        self.textColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
