//
//  TextField.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 07/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit

class TextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderColor = #colorLiteral(red: 0.1921568662, green: 0.007843137719, blue: 0.09019608051, alpha: 1)
        layer.borderWidth = 1
        layer.cornerRadius = 3
        self.leftView = UIView(frame: .init(x: 0, y: 0, width: 5, height: 1))
        self.leftViewMode = .always
    }

    override var intrinsicContentSize: CGSize {
        return .init(width: 100, height: 45)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
