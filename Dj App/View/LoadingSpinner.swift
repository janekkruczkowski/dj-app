//
//  LoadingSpinner.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 08/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import UIKit

class LoadingSpinner: UIActivityIndicatorView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.style = .gray
        self.startAnimating()

    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}


extension UIViewController {
    func setupSpinner(spinner: UIActivityIndicatorView) {
        self.view.addSubview(spinner)
        spinner.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 30, height: 30))
            make.center.equalTo(view.safeAreaLayoutGuide)
        }

    }

    func hideLoadingIndicator(spinner: UIActivityIndicatorView?) {
        guard let spinner = spinner else {
            return
        }
        spinner.stopAnimating()
        spinner.isHidden = true
    }
}
