//
//  Error.swift
//  Dj App
//
//  Created by Janek Kruczkowski on 07/04/2019.
//  Copyright © 2019 Janek Kruczkowski. All rights reserved.
//

import Foundation

enum ApiError: Error {
    case noData
}

extension ApiError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noData:
            return NSLocalizedString("Brak danych.", comment: "No Data")
        }
    }
}
